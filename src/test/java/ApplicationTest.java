import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ApplicationTest {

    private final String INPUT_RESOURCE_PATH = "src\\test\\resources\\input\\";
    private final String EXPECTED_RESOURCE_PATH = "src\\test\\resources\\expected\\";

    @ParameterizedTest
    @CsvSource({
            "default_text_test.txt, mapped_default_text_text.txt",
            "large_letters_test.txt, mapped_large_letters_test.txt"})
    void letterWordsMappingTest(String exploreFileName, String expectedFileName){
        // GIVEN
        String filePathToExplore = INPUT_RESOURCE_PATH + exploreFileName;
        String textToExplore = Application.readTextFromResource(filePathToExplore);
        String filePathToCompare = EXPECTED_RESOURCE_PATH + expectedFileName;
        String expectedWords = Application.readTextFromResource(filePathToCompare);

        // WHEN
        Map<Character, Set<String>> dataAfterMapping = Application.letterWordsMapping(textToExplore);

        // THEN
        assertEquals(expectedWords, dataAfterMapping.toString());
    }

    @ParameterizedTest
    @CsvSource({
            "unnecessary_symbols_test.txt, after_remove_unnecessary_symbols_test.txt",
            "useful_words_test.txt, expected_useful_words_test.txt"})
    void removeUnnecessarySymbolsFromTextTest(String exploreFileName, String expectedFileName) {
        // GIVEN
        String filePathToExplore = INPUT_RESOURCE_PATH + exploreFileName;
        String textToExplore = Application.readTextFromResource(filePathToExplore);
        String filePathToCompare = EXPECTED_RESOURCE_PATH + expectedFileName;
        String expectedWords = Application.readTextFromResource(filePathToCompare);

        String[] wordsToExploreArray = textToExplore.split(" ");
        Queue<String> wordsAfterRemoveSymbols = new LinkedList<>();

        // WHEN
        for (String word: wordsToExploreArray) {
            wordsAfterRemoveSymbols.add(Application.removeUnnecessarySymbols(word));
        }

        // THEN
        assertEquals(expectedWords, wordsAfterRemoveSymbols.toString());
    }

    @Test
    void uppercaseLettersTest() {
        // GIVEN
        String filePathToExplore = INPUT_RESOURCE_PATH + "large_letters_test.txt";
        String textToExplore = Application.readTextFromResource(filePathToExplore);
        String filePathToCompare = EXPECTED_RESOURCE_PATH + "lowercase_large_letters_test.txt";
        String expectedText = Application.readTextFromResource(filePathToCompare);

        // WHEN
        String lowerCaseText = Application.getLowercaseText(textToExplore);

        // THEN
        assertEquals(expectedText,lowerCaseText);
    }

    @Test
    void readTextFromFileTest(){
        // GIVEN
        String filePath = INPUT_RESOURCE_PATH + "default_text_test.txt";
        String expectedText = "ala ma kota, kot koduje w Javie kota";

        // WHEN
        String textFromResource = Application.readTextFromResource(filePath);

        // THEN
        assertEquals(expectedText, textFromResource);
    }
}
