import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Aplikacja indeksująca litery występujące w tekscie oraz mapująca je do zbioru słów, w których występują.
 */
public class Application {

    public static void main(String[] args) {
        String proposedTextFilePath = "src\\main\\resources\\proposed_text.txt";
        String text = readTextFromResource(proposedTextFilePath);
        letterWordsMapping(text);
    }

    static String readTextFromResource(String path) {
        File file = new File(path);
        Scanner sc = null;
        try {
            sc = new Scanner(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        StringBuilder fileText = new StringBuilder();
        while (sc != null && sc.hasNextLine()){
            fileText.append(sc.nextLine());
        }
        return fileText.toString();
    }

    static Map<Character, Set<String>> letterWordsMapping(String input) {
        System.out.println("Dane wejsciowe: " + input);

        input = getLowercaseText(input);
        String[] wordsArray = input.split(" ");

        Set<String> words = Arrays.stream(wordsArray)
                .map(Application::removeUnnecessarySymbols)
                .collect(Collectors.toSet());

        Set<Character> alphabetLetters = input.chars()
                .filter(Character::isLetter)
                .mapToObj(letter -> (char) letter)
                .collect(Collectors.toCollection(TreeSet::new));

        Map<Character, Set<String>> letterWordsMapping = new HashMap<>();
        for (Character letter : alphabetLetters) {
            Set<String> wordsContainLetter = words.stream()
                    .filter(word -> word.contains(letter.toString()))
                    .collect(Collectors.toCollection(TreeSet::new));
            letterWordsMapping.put(letter, wordsContainLetter);
        }
        printMap(letterWordsMapping);

        return letterWordsMapping;
    }

    static String getLowercaseText(String input) {
        return input.toLowerCase();
    }

    static String removeUnnecessarySymbols(String word) {
        int firstLetterPosition = 0;
        int lastLetterPosition = word.length();

        for (int i = 0; i < word.length(); i++) {
            if (Character.isLetter(word.charAt(i))) {
                firstLetterPosition = i;
                break;
            }
        }
        for (int i = word.length() - 1; i >= 0; i--) {
            if (Character.isLetter(word.charAt(i))) {
                lastLetterPosition = i;
                break;
            }
        }
        lastLetterPosition = (lastLetterPosition != word.length()) ? (lastLetterPosition + 1) : lastLetterPosition;
        return word.substring(firstLetterPosition, lastLetterPosition);
    }

    private static void printMap(Map<Character, Set<String>> letterWordsMapping) {
        System.out.println("Dane wyjsciowe:");
        for (Character key : letterWordsMapping.keySet()) {
            System.out.print(key + ":");

            StringBuilder sb = new StringBuilder();
            for (String word : letterWordsMapping.get(key)) {
                sb.append(" ").append(word).append(",");
            }
            sb.deleteCharAt(sb.length() - 1);
            sb.append("\n");
            System.out.print(sb);
        }
    }
}
